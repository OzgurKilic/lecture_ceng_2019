package exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class HandleExceptionDemo {

	public static void main(String[] args) {
		int count = 0;
		int total = 0;
		Scanner scanner = new Scanner(System.in);
		
		while (count < 5) {

			System.out.println("Please enter number " + (count + 1) + " :");
			int num = 0;
			try {
				num = readInt(scanner);
				count++;
				total += num; 

			}catch(InputMismatchException ex) {
				System.out.println("Not a valid number");
				scanner.nextLine();
			}
		}

		System.out.println("Average of nums are : " + total/5);
		

	}

	private static int readInt(Scanner scanner) {
		try {
			int num;
			num = scanner.nextInt();
			return num;
		}catch(InputMismatchException ex) {
			System.out.println("Exception handled");
			//scanner.nextLine();
			throw ex;
		}
	}

}
