package exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CheckedExceptionDemo {

	public static void main(String[] args) {
		try {
			openFile();
		}catch (FileNotFoundException e) {
			if ( e instanceof FileNotFoundException) {
				System.out.println("FileNotFound Exception received");
			}
			//Recovery code
		} catch (Throwable e) {
			if ( e instanceof FileNotFoundException) {
				System.out.println("FileNotFound Exception received");
			}
			//Recovery code
		}
	}

	private static void openFile(){
		
			File file = new File("MyFile.txt");
			
			Scanner inputFile = new Scanner(file);
			
	}

}
