package shapes;

import drawing.Drawable;

public  abstract class Shape implements Drawable{
	
	String color;
	
	public abstract double area() ;

}
