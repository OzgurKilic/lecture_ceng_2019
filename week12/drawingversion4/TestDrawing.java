package drawingversion4;



import drawing.Text;
import shapes.Circle;
import shapes.Rectangle;
import shapes.Shape;
import shapes.Square;

public class TestDrawing {

	public static void main(String[] args) {
		
		//Shape shape = new Shape();
		
		Drawing drawing = new Drawing();
		
		drawing.addDrawable(new Circle(5));
		drawing.addDrawable(new Rectangle(5,6));
		drawing.addDrawable(new Square(5));
		//drawing.addShape("Hello");
		
		//Shape s = new Shape();

		System.out.println("Total area = " + drawing.calculateTotalArea());
		
		
		drawing.addDrawable(new Text("Hello"));
		
		drawing.draw();
		
	}

}
