package drawingversion4;

import java.util.ArrayList;

import drawing.Drawable;
import shapes.Shape;

public class Drawing {

	private ArrayList<Drawable> drawables = new ArrayList<Drawable>();

	public double calculateTotalArea() {
		double totalArea = 0;

		for (Drawable dr : drawables) {
			if (dr instanceof Shape)
				totalArea += ((Shape) dr).area();
		}

		return totalArea;
	}

	public void addDrawable(Drawable s) {
		drawables.add(s);
	}

	public void draw() {
		for (Drawable drawable : drawables) {
			drawable.draw();
		}
	}

}
