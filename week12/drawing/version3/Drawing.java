package drawing.version3;

import java.util.ArrayList;

import drawing.Text;
import shapes.Shape;

public class Drawing {

	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	private ArrayList<Text> texts = new ArrayList<Text>();

	public double calculateTotalArea() {
		double totalArea = 0;

		for (Shape shape : shapes) {
			totalArea += shape.area();
		}

		return totalArea;
	}

	public void addShape(Shape s) {
		shapes.add(s);
	}

	public void draw() {
		for (Shape shape : shapes) {
			//shape.draw();
		}
		for (Text text : texts) {
			text.draw();
		}

	}

}
