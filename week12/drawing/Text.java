package drawing;

import java.io.Serializable;

public class Text extends Object implements Drawable, Serializable{

	private String text;

	public Text(String text) {
		this.text = text;
	}

	public void draw() { 
		System.out.println("Drawing " + text);
	}

}
