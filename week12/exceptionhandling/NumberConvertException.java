package exceptionhandling;

public class NumberConvertException extends Exception {
	
	public NumberConvertException() {
		super("Only numbers between 0 and 9 are convertible");
	}

}
