package exceptionhandling;

public class NumberConverter {
	

	String[] numbers = {"Zero", "One", "Two","Three","Four","Five","Six","Seven","Eight","Nine"};
	
	
	public  String convert(int number) throws NumberConvertException {
		if (number > 9 || number <0)
			throw new NumberConvertException();
		return numbers[number];
	}

	public static void main(String[] args) {
		try {
			foo(5);
		}catch(NumberConvertException nce) {
			System.out.println(nce.getMessage());
		}
		
	}
	
	public static void foo(int num) throws NumberConvertException {
		NumberConverter converter = new NumberConverter();
		System.out.println(converter.convert(num));
		
	}

	
}
