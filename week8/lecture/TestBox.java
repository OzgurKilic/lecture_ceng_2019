package lecture;

import generics.Box;

public class TestBox extends Object{

	public static void main(String[] args) {
		
		Rectangle r = new Rectangle(4,7);
		r.length = 8;
		r.width = 5;
		
		System.out.println(r);

		
		System.out.println("Area rect: " + r.area());
				
		Box box = new Box();
		box.length = 6;
		box.width = 5;
		box.height= 9;
		
		
		System.out.println("Volume box :" + box.volume());
		
		System.out.println("Area box :" + box.area());
	}

}
