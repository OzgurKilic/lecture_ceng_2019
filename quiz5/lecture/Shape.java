package lecture;

public abstract class Shape {
	
	public abstract double area();

	
	public abstract double perimeter();
}
