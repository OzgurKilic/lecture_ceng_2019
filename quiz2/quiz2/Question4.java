package quiz2;

public class Question4 {

	public static void main(String[] args) {
		int [][] matrix1 = {{2,3},{5,1},{3,4}};
		int [][] matrix2 = {{1,3},{3,1},{5,4}};
		
		
		
		
		int[][] sum = add(matrix1, matrix2);
		
		for (int i= 0; i<sum.length; i++) {
			for (int j=0; j<sum[i].length; j++) {
				System.out.print(sum[i][j] + " ");
			}
			System.out.println();
		}		
	
	}
	
	
	//Write a method that calculates matrix addition, parameters of the method are the matrices that will be added
	//Your method will return the resulting matrix, assume that the size of the input matrices are the same 
	public static int[][] add(int[][] matrix1, int[][] matrix2){
		int rowCount = matrix1.length;
		int colCount = matrix1[0].length;
		
		int[][] result = new int[rowCount][colCount];
		
		
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				result[i][j] = matrix1[i][j] + matrix2[i][j];
			}
		}
		
	
		return result;
	
	}
}
