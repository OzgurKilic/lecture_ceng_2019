package quiz2;

public class Question6 {

	public static void main(String[] args) {

//		Write a recursion function which calculates the the value of base to the n power.
//		Below find example method calls and their return values. (12 Pts)
		System.out.println(powerN(3, 0));
		System.out.println(powerN(3, 2));
		System.out.println(powerN(5, 4));
		
	}

	public static int powerN(int base, int power) {
		if (power == 0)
			return 1;
		return base * powerN(base, power-=1);
	}
	
	

}
