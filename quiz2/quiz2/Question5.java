package quiz2;

public class Question5 {

	public static void main(String[] args) {

		//What would be the return value of the below function for function(6)?
		//for (int i=0; i< 7; i++)
			//System.out.println(function(i));
		
		System.out.println(function(6));
	}

	public static int function(int n) {
		if (n == 0)
			return 0;
		else if (n == 1)
			return 1;
		else
			return 2 * function(n - 1) +  function(n - 2);
	}

}
