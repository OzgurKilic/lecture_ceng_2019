package lecture;

public class FinallyDemo {

	public static void main(String[] args) {
	
		System.out.println("method A will be executed");
		
			methodA();
		
			System.out.println("method A succefully executed");
		
		

	}

	private static void methodA() {
		System.out.println("method B will be executed");
		try {
			methodB();
		}catch(Exception ex) {
			System.out.println("Handled in A");
		}
		System.out.println("method B succefully executed");
	}

	
	
	private static void methodB()throws Exception{
		try {
			throw new Exception();
		}catch(Exception ex) {
			System.out.println("Caught the Exception");
			return;
		}finally {

			System.out.println("divided successfully");
		}
		
	}

}
