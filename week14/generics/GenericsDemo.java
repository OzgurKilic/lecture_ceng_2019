package generics;

import java.util.ArrayList;

public class GenericsDemo {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		
		list.add("Hello");
		list.add("Hi");

		
		String str = list.get(0);
		
		//str = 5;
		

		Box<String> box = new Box<>("Hello");
		
		//box.setObj(5); //accidentally
		
		str = box.getObj();
		
		Box<Integer> boxInt = new Box<>(8);
		
		
		Box b = new Box(6.5);
		
		Pair<Integer,String> pair = new Pair<>(5,"Hello");
		
		Pair<Integer,Integer> pairInt = new Pair<>(5,6);
		
		Pair<String,Integer> pair5 = new Pair<>("Hi",6);
		
		Pair<Integer, Box<String>> pair3;
		
		Box<Box<Box<String>>> box3;
		
		
		Pair pairRaw = new Pair(3,4);
		
		GenericsDemo.<Integer,String>compare(pair, pair5 );
	}
	
	public static <K,V> Box<V>  compare(Pair<K,V> b1, Pair<V,K> b2){
	
		return b1.getBoxValue();
	}
	

}
