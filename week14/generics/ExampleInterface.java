package generics;

public interface ExampleInterface <T,K,R>{

	T methodA(K a, R b);
	
}
