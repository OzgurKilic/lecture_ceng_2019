package quiz6;

import java.util.ArrayList;
import java.util.List;

public class StackImpl<T> implements Stack<T> {

	ArrayList<T> list = new ArrayList<T>();
	
	@Override
	public void push(T obj) {
		list.add(obj);	
	}

	@Override
	public T pop() {
		return list.remove(list.size() -1);
	}

	@Override
	public boolean empty() {
		return list.size() == 0;
	}

	@Override
	public void pushAll(Stack<T> stack) {
		Stack<T> stackTemp = new StackImpl<>();
		while(!stack.empty()) {
			T t = stack.pop();
			stackTemp.push(t);
		}
		while(!stackTemp.empty()) {
			T t = stackTemp.pop();
			stack.push(t);
			push(t);
		}
	}

	
	@Override
	public int size() {
		
		return list.size();
	}
	
	public String toString() {
		return list.toString();
	}
	

}
