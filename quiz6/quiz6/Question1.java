package quiz6;

public class Question1 {
	public static void main(String [] args) {
        try {
            badMethod();  
            System.out.print("A"); 
        }catch (Exception ex){
            System.out.print("B");
            return;
        }catch (Throwable ex){
            System.out.print("C");
           
        }finally{
            System.out.print("D"); 
        } 
        System.out.print("E"); 
    }  
    public static void badMethod(){
        throw new Error(); 
    } 
    
}
