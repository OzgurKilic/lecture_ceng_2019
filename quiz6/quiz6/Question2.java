package quiz6;

public class Question2 {
	public static void main(String[] args) {
		try{
			method1();
		}catch(RuntimeException ex) {
			System.out.println("H");
		}catch(Exception ex) {
			System.out.println("I");
		}
	}
	private static void method1() {
		System.out.print("A");
		try{
			method2();
		}catch(NullPointerException ex){
			System.out.print("D");
			if (0==0)
				throw new IllegalArgumentException(ex);
			System.out.print("E");
		}finally{
			System.out.print("F");	
		}
		System.out.print("G");
	}
	private static void method2() {
		System.out.print("B");
		String a =null;
		a.length();	
		System.out.print("C");
	}
	
}
