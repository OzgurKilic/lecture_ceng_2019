package quiz6;

public class TestStack {

	public static void main(String[] args) {
		Stack<String> stack1 = new StackImpl<>();
		stack1.push("A");
		stack1.push("B");

		Stack<String> stack2 = new StackImpl<>();
		stack2.push("X");
		stack2.push("Y");

		
		stack1.pushAll(stack2);
		
		System.out.println("size of stack1 " + stack1);
		System.out.println("size of stack2 " + stack2);
	}

	
	
}

