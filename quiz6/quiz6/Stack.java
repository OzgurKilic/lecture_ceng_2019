package quiz6;

public interface Stack<T> {
	
	void push(T obj);
	public T pop();
	boolean empty();
	public void pushAll(Stack<T> stack);
	
	int size();

	
}
