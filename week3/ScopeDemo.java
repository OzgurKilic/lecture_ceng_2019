

public class ScopeDemo {

	public static void main(String[] args) {

		int x = 9;
		int y = 11;
		if (x == 10) {
			y = 5;
			System.out.println("Both variables are known here: " + x + " and " + y);
		}
		y = y + 1; // Compilation Error System.out.println("The variable y is not known here!");
		System.out.println("Both variables are known here: " + x + " and " + y);
	}

}
