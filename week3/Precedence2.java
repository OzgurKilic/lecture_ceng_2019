

public class Precedence2 {

	public static void main(String[] args) {
		int i = 2;
		boolean bool = (i > 5) && (i++ > 3) && (i++ < 3);
		System.out.println("bool = " + bool + " i = " + i);
	}

}
