

public class Scope {
	public static void main(String[] args){ 		
		int x = 5;
		int y = 5;
		if (x == 5){			
			//int x = 6;  //Error, x already exists in this scope			
			y = 72;			
			System.out.println("x = " + x + " y = " + y);		
		} 		
		
		//System.out.println("x = " + x + " y = " + y);  //Error	
	}
}