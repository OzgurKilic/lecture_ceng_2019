
public class ArrayDemo {

	public static void main(String[] args) {
		int[] arr;
		arr = new int[8];
		
		arr[0] = 1;

		arr[1] = arr[0] + 1;
		
		arr[6] = 8;
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		
		boolean[] bools = new boolean[5];
		
		for (int i = 0; i < bools.length; i++) {
			System.out.println(bools[i]);
		}
		
	}

}
