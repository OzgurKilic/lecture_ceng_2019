package lecture;

public class Point2D {
	
	protected int x,y;

	public Point2D(int x, int y) {
		this.x = x; this.y = y;
	}
	
	public void move (int deltaX, int delytaY) {
		x+=deltaX; y+=delytaY;
	}
	
	public String toString() {
		return "x = " + x + ", y = " + y;
	}
	
	public int  distanceSquareFromOrigin() {
		return x*x + y*y;
	}
}
