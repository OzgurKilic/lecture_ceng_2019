package lecture;

public class Point3D extends Point2D{
	
	int z;
	
	public Point3D(int x, int y, int z) {
		super(x,y);

		this.z = z;
	}
	
	public void move (int deltaX, int delytaY, int deltaZ) {
		super.move(deltaX, delytaY);
		this.z += deltaZ; 
	}
	
	public String toString() {
		return super.toString() + ", z = " + z;
	}

	
	public int  distanceSquareFromOrigin() {
		return super.distanceSquareFromOrigin() + z*z;
	}
}
