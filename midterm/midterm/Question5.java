package midterm;

public class Question5 {
	
	public static void main(String[] args) {
		
		System.out.println(divide(0,8));
		
		
	}
	
	public static int divide(int divident, int divisor) {
	    if (divident < divisor )
	    	return 0;
	    else 
	    	return 1 + divide(divident - divisor, divisor);
	 }

}
