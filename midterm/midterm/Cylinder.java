package midterm;

public class Cylinder {
	
	private Circle circle;
	
	private Rectangle rectangle;

	public Cylinder(int radius, int height) {
		circle = new Circle(radius);
		rectangle = new Rectangle(height, circle.perimeter());
	}
	
	public double area() {
		return  2 * circle.area() + rectangle.area();
	}
	
	public double volume() {
		return  circle.area() * rectangle.getLength();
	}	

}
