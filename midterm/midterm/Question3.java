package midterm;

public class Question3 {
	
	public static int secondMax(int[] array){
		int secondLargest = -1;
		if (array.length > 1){
			int max = array[0];
			for (int i = 1; i< array.length; i++ ){
				if(array[i] == max){
					continue;
				}else if (array[i] > max){
					secondLargest = max;
					max = array[i];
				}else if (array[i] > secondLargest){
					secondLargest = array[i];
				}	
			}
		}
		return secondLargest;
	}

}
