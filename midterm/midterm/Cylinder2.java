package midterm;

public class Cylinder2  extends Circle{
	
	private double height;
	

	public Cylinder2(int radius, int height) {
		super(radius);
		this.height = height;
	}
	
	public double area() {
		return  2 * super.area() + super.perimeter() * height;
	}
	
	public double volume() {
		return  super.area() * height;
	}	

	public String toString() {
		return super.toString() + ", height = " + height;
	}
	
}
