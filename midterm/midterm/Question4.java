package midterm;

public class Question4 {
	
	public static void main(String[] args) {
		
		for (int i = 0; i<50; i++) {
			System.out.println( i + " -> " + golden(i));
		}
		
	}
	
	public static double golden(int n) {
	    if (n == 0) return 1;
	    return 1 + 1 / golden(n-1);
	 }

}
