package midterm;

public class Question2 {
	
	public static void main(String[] args) {
		System.out.println(min (3,5,3,5));
	}
	
	private static int min(int a, int b, int c, int d) {
		int min = a;
		 if (b < min)
			 min = b;
		 if (c < min)
			 min = c;
		 if (d < min)
			 min = d;
		return min;
	}

}
