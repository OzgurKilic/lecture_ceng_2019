package lecture;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.lang.Math;

public class ArrayListDemo {
	
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList();
		
		double x = Math.PI;
		
		Random r = new Random();
		r.nextInt(9);
		
		list.add("Hello");
		list.add("Merhaba");
		list.add("Hola");
		list.add("Merhaba");
		
		for(String str : list) {
			System.out.println(str);
		}
	
		System.out.println();
		
		HashSet<String> set = new HashSet<>();
		
		set.add("Hello");
		set.add("Merhaba");
		set.add("Hola");
		set.add("Merhaba");
		
		for(String str: set) {
			System.out.println(str);
		}
		
		
	}

}
