package lecture;

public class Overload {
	
	int a;
	static int b;

	public static void main(String[] args) {
		Overload ovl = new Overload();
		ovl.a  = 5;
		ovl.ovlDemo(6);
		ovl.ovlDemo(6.0);
		
		Overload.b = 9;
		
		Overload.ovlDemo("Hello");
		
	}
	
	public void ovlDemo() {
		a = 6;
		b= 9;
		System.out.println("ovlDemo");
	}

	
	public void ovlDemo(int a) {
		System.out.println("ovlDemo " + a);
	}

	public void ovlDemo(double a) {
		System.out.println("ovlDemo " + a);
	}
	
	public void ovlDemo(int a, int b) {
		Overload.b = 9;
		System.out.println("ovlDemo " + a + " " + b);
	}
	
	public static void ovlDemo(int a, double b) {
		System.out.println("ovlDemo " + a + " " + b);
	}
	
	public static void ovlDemo(String x) {
		b = 9;
		System.out.println("ovlDemo " + x);
	}
	
}
