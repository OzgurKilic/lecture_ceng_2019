package lecture;

public class ParameterPassing {

	int z;
	
	public static void main(String[] args) {
		
		String str = new String("Hello");
		
		str = foo(str);
		System.out.println("foo: str = " + str);
		
		ParameterPassing p = new ParameterPassing();
		p.z = 5;
		
		foo2(p);
		
		System.out.println("foo2: p.z = " + p.z);
		
		
		
	}
	
	public  static void foo2(ParameterPassing p) {
		p =  new ParameterPassing();
		p.z = 3;
		System.out.println("foo2: p.z = " + p.z);
	}	
		
	
	public   static String foo(String str) {
		str = "Merhaba";
		System.out.println("foo: str = " + str);
		return str;
	}

	
	
	
}
