
public abstract class Shape {
	
	String color;
	
	public abstract  void draw();
	
	public abstract double calculateArea();
	
	public final void invertColor() {
		System.out.println("Inversting Color");
	}

}
