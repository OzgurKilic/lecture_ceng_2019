

public class DrawingTest {

	public static void main(String[] args) {
		Circle c = new Circle();
		Rectangle r = new Rectangle();
		
		//Shape s = new Shape();  Abstract class can not be instantiated
		
		c.draw();
		r.draw();

	}

}
