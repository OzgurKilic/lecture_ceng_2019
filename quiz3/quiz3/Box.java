package quiz3;

public class Box {

	private int height, length, width;
	
	public static int count;

	public Box(int height, int length, int width) {
		this.height = height;
		this.length = length;
		this.width = width;
	}
	
	public int area() {
		return 2*height*width + 2*height*length + 2*length*width;
	}
	
	
	public int volume() {
		return height*width*length;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	
	
}
